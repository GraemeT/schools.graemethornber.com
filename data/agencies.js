var agencies = [
	{
		'name':'BallantyneTaylor',
		'url':'http://ballantynetaylor.co.nz/',
		'address':[
			{
				'street':'30 St Benedicts Street',
				'suburb':'Newton',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'BBT Digital',
		'url':'http://bbtdigital.com/',
		'address':[
			{
				'street':'1 College Hill',
				'suburb':'Freemans Bay',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Calibrate',
		'url':'https://www.calibrate.co.nz',
		'address':[
			{
				'street':'80 College Hill',
				'suburb':'Freemans Bay',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Catch',
		'url':'http://www.catchdesign.co.nz/',
		'address':[
			{
				'prefix':'Level 4, Intech House',
				'street':'17-19 Garrett Street',
				'suburb':'Te Aro',
				'city':'Wellington',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Colenso BBDO',
		'url':'http://www.colensobbdo.co.nz/',
		'address':[
			{
				'street':'100 College Hill',
				'suburb':'Ponsonby',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Cucumber',
		'url':'http://www.cucumber.co.nz',
		'address':[
			{
				'street':'3 Glenside Crescent',
				'suburb':'Eden Terrace',
				'city':'Auckland',
				'country':'NZ'
			},
			{
				'street':'78 Second Ave',
				'suburb':'CBD',
				'city':'Tauranga',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Digital Arts Network',
		'url':'http://www.dan.co.nz/',
		'address':[
			{
				'street':'11 Mayoral Drive',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Digital Stream',
		'url':'http://www.digitalstream.co.nz/',
		'address':[
			{
				'prefix':'Studio 4',
				'street':'300 Richmond Road',
				'suburb':'Grey Lynn',
				'city':'Auckland',
				'country':'NZ'
			},
			{
				'street':'393B Grey Street',
				'suburb':'Hamilton East',
				'city':'Hamilton',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Fracture',
		'url':'http://www.fracture.co.nz/',
		'address':[
			{
				'prefix':'Level 2',
				'street':'29 Beach Road',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Grand',
		'url':'http://grand.co.nz/',
		'address':[
			{
				'street':'20 Garfield Street',
				'suburb':'Parnell',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Gravitate',
		'url':'http://www.gravitate.co.nz/',
		'address':[
			{
				'Unit 2a, Heritage Hotel Building',
				'street':'Cnr Hobson & Wyndham',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Hot Mustard',
		'url':'http://www.hotmustard.co.nz',
		'address':[
			{
				'street':'17 Esmonde Rd',
				'suburb':'Takapuna',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Kiwise',
		'url':'https://www.kiwise.com',
		'address':[
			{
				'prefix':'Level 7A',
				'street':'17 Albert Street',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Little Giant',
		'url':'https://www.littlegiant.co.nz',
		'address':[
			{
				'prefix':'3F / Level 3',
				'street':'47 High St',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Mediar',
		'url':'http://www.mediar.co.nz',
		'address':[
			{
				'street':'5 Dockside Lane',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Netfinity',
		'url':'http://netfinity.co.nz',
		'address':[
			{
				'street':'15 Sale Street',
				'suburb':'Freemans Bay',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Ogilvy & Mather',
		'url':'http://www.ogilvy.co.nz',
		'address':[
			{
				'street':'22 Stanley Street',
				'suburb':'Parnell',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'RAPP (DDB)',
		'url':'http://www.rapp.co.nz',
		'address':[
			{
				'prefix':'Level 6',
				'street':'80 Greys Ave',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Reach',
		'url':'http://reachsolutions.co.nz/',
		'address':[
			{
				'street':'12 South Park Place',
				'suburb':'Penrose',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Saatchi & Saatchi',
		'url':'http://saatchi.co.nz',
		'address':[
			{
				'street':'123-125 The Strand',
				'suburb':'Parnell',
				'city':'Auckland',
				'country':'NZ'
			},
			{
				'street':'Clyde Quay Wharf',
				'suburb':'Te Aro',
				'city':'Wellington',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Salt Interactive',
		'url':'http://www.saltinteractive.com/',
		'address':[
			{
				'street':'91 Cook Street',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Satellite Media',
		'url':'http://www.satellitemedia.co.nz/',
		'address':[
			{
				'prefix':'Level 3',
				'street':'65-73 Parnell Rise',
				'suburb':'Parnell',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Shine',
		'url':'http://shinelimited.co.nz/',
		'address':[
			{
				'street':'116-18 Quay Street',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Special Group Limited',
		'url':'http://www.specialgroup.co.nz/',
		'address':[
			{
				'country':'NZ',
				'email':'frontdesk@specialgroup.co.nz',
				'contact':[
					{
						'name':'Michael Redwood',
						'title':'Managing Partner',
						'email':'michael@specialgroup.co.nz'
					},
					{
						'name':'Tony Bradbourne',
						'title':'Creative Director, Art Direction',
						'email':'Tony@specialgroup.co.nz'
					},
					{
						'name':'Rob Jack',
						'title':'Creative Director, Copy',
						'email':'Rob@specialgroup.co.nz'
					},
					{
						'name':'Heath Lowe',
						'title':'Creative Director, Design',
						'email':'Heath@specialgroup.co.nz'
					}
				]
			},
			{
				'street':'270 Devonshire Street',
				'suburb':'Surry Hills',
				'city':'Sydney',
				'country':'AU',
				'contact':[
					{
						'title':'General Enquiries',
						'phone':'02 9037 1899',
						'email':'frontdesk@specialgroup.com.au'
					},
					{
						'name':'Cade Heyde',
						'title':'Recruitment',
						'phone':'0402 016 611',
						'email':'cade@specialgroup.com.au'
					},
					{
						'name':'Lindsey Evans',
						'title':'PR & New Business',
						'phone':'0404 036 601',
						'email':'lindsey@specialgroup.com.au'
					}
				]
			}
		]
	},
	{
		'name':'Spitfire',
		'url':'http://www.spitfire.co.nz/',
		'address':[
			{
				'prefix':'Level 2',
				'street':'25 Crummer Road',
				'suburb':'Ponsonby',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Storm IMC',
		'url':'http://stormimc.co.nz/',
		'address':[
			{
				'prefix':'6/',
				'street':'10 Omega Street',
				'suburb':'Rosedale',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Sugar & Partners',
		'url':'http://sugarandpartners.co.nz',
		'address':[
			{
				'prefix':'Level 3',
				'street':'8-10 Beresford Square',
				'suburb':'Newton',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'The Space InBetween',
		'url':'http://thespaceinbetween.co.nz/',
		'address':[
			{
				'street':'3 Centre Street',
				'suburb':'Freemans Bay',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Touchcast',
		'url':'http://www.touchcast.co.nz',
		'address':[
			{
				'street':'33 College Hill',
				'suburb':'Freemans Bay',
				'city':'Auckland',
				'country':'NZ'
			}
			{
				'prefix':'Level 2',
				'street':'1 Post Office Square',
				'suburb':'CBD',
				'city':'Wellington',
				'country':'NZ'
			}
		]
	},
	{
		'name':'True',
		'url':'http://thisistrue.co/',
		'address':[
			{
				'street':'449 Richmond Road',
				'suburb':'Grey Lynn',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Y&R',
		'url':'http://yr.co.nz/',
		'address':[
			{
				'prefix':'Shed 4, City Works Depot',
				'street':'90 Wellesley Street West',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			},
			{
				'street':'81 Abel Smith Street',
				'suburb':'Te Aro',
				'city':'Wellington',
				'country':'NZ'
			}
		]
	},
	{
		'name':'Zoo',
		'url':'http://zoogroup.com/',
		'address':[
			{
				'street':'Elgin Street',
				'suburb':'Grey Lynn',
				'city':'Auckland',
				'country':'NZ'
			},
			{
				'prefix':'Level 2',
				'street':'32 Garden Street',
				'suburb':'South Yarra',
				'city':'Melbourne',
				'country':'AU'
			},
			{
				'street':'77-83 William Street',
				'suburb':'Darlinghurst',
				'city':'Sydney',
				'country':'AU'
			},
			{
				'prefix':'Level 3',
				'street':'59 Wentworth Avenue',
				'suburb':'Kingston',
				'city':'Canberra',
				'country':'AU'
			},
			{
				'street':'137 Cecil Street',
				'suburb':'Singapore',
				'city':'Singapore',
				'country':'SG'
			}
		]
	},
	{
		'name':'99',
		'url':'http://www.99.co.nz/',
		'address':[
			{
				'street':'318 Richmond Road',
				'suburb':'Grey Lynn',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'',
		'url':'',
		'address':[
			{
				'street':'',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'',
		'url':'',
		'address':[
			{
				'street':'',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'',
		'url':'',
		'address':[
			{
				'street':'',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'',
		'url':'',
		'address':[
			{
				'street':'',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	},
	{
		'name':'',
		'url':'',
		'address':[
			{
				'street':'',
				'suburb':'CBD',
				'city':'Auckland',
				'country':'NZ'
			}
		]
	}

	
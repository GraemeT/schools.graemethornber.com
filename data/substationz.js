var substationz = [
    {
        'id':'8800',
        'address':'285 East Tamaki Road',
        'suburb':'Otara',
        'lat':'-36.960373',
        'lng':'174.889266'
    },
    {
        'id':'8801',
        'address':'10 Udys Road',
        'suburb':'Pakuranga',
        'lat':'-36.908959',
        'lng':'174.884972'
    },
    {
        'id':'8802',
        'address':'103 Wiri Station Road',
        'suburb':'Manukau',
        'lat':'-37.000448',
        'lng':'174.868728'
    },
    {
        'id':'8803',
        'address':'1 Laxon Avenue',
        'suburb':'Otara',
        'lat':'-36.955533',
        'lng':'174.859686'
    },
    {
        'id':'8804',
        'address':'80 Raglan Street',
        'suburb':'Papatoetoe',
        'lat':'-36.970984',
        'lng':'174.827017'
    },
    /*{
        'id':'8805',
        'address':'140R­-142 Boundary Road',
        'suburb':'Clover Park',
        'concealed':'yes',
        'lat':'-36.984308',
        'lng':'174.900627'
    },*/
    {
        'id':'8806',
        'address':'39 Whitford Road',
        'suburb':'Howick',
        'lat':'-36.908522',
        'lng':'174.928606'
    },
    {
        'id':'8807',
        'address':'142 Maraetai Drive',
        'suburb':'Maraetai',
        'lat':'-36.879149',
        'lng':'175.035561'
    },
    {
        'id':'8808',
        'address':'28 Weymouth Road',
        'suburb':'Manukau',
        'lat':'-37.025071',
        'lng':'174.895321'
    },
    {
        'id':'8809',
        'address':'15 Nandina Avenue',
        'suburb':'Greenmount',
        'lat':'-36.929433',
        'lng':'174.898895'
    },
    {
        'id':'8810',
        'address':'418 Roscommon Road',
        'suburb':'Manurewa',
        'lat':'-37.026682',
        'lng':'174.866735'
    },
    {
        'id':'8811',
        'address':'72 Hutchinson Road,',
        'suburb':'Howick',
        'lat':'-36.893950',
        'lng':'174.912975'
    },
    {
        'id':'8812',
        'address':'2 Canning Crescent,',
        'suburb':'Mangere',
        'lat':'-36.969953',
        'lng':'174.802835'
    },
    {
        'id':'8813',
        'address':'1 Gillies Avenue',
        'suburb':'Newmarket',
        'lat':'-36.869247',
        'lng':'174.774604'
    },
    {
        'id':'8814',
        'address':'355 Rosebank Road',
        'suburb':'Avondale',
        'lat':'-36.883325',
        'lng':'174.681347'
    },
    {
        'id':'8815',
        'address':'867B New North Rd',
        'suburb':'Mt Albert',
        'lat':'-36.883156',
        'lng':'174.715893'
    },
    {
        'id':'8816',
        'address':'2­-4 Minto Road',
        'suburb':'Remuera',
        'lat':'-36.880785',
        'lng':'174.812384'
    },
    {
        'id':'8817',
        'address':'60 College Road',
        'suburb':'St Johns',
        'lat':'-36.879371',
        'lng':'174.841370'
    },
    {
        'id':'8818',
        'address':'98 Franklin Road',
        'suburb':'Freemans Bay',
        'lat':'-36.849174',
        'lng':'174.751169'
    },
    {
        'id':'8819',
        'address':'12 Dundonald Street',
        'suburb':'Eden Terrace',
        'lat':'-36.864472',
        'lng':'174.760019'
    },
    {
        'id':'8820',
        'address':'27­-29 Ruskin Street',
        'suburb':'Parnell',
        'lat':'-36.853659',
        'lng':'174.781926'
    },
    {
        'id':'8821',
        'address':'4 Thatcher Street',
        'suburb':'Mission Bay',
        'lat':'-36.861185',
        'lng':'174.833674'
    },
    {
        'id':'8822',
        'address':'58­-60 Kesteven Avenue',
        'suburb':'Glendowie',
        'lat':'-36.855035',
        'lng':'174.868065'
    },
    {
        'id':'8823',
        'address':'1094 Great North Road',
        'suburb':'Point Chevalier',
        'lat':'-36.869171',
        'lng':'174.714296'
    },
    {
        'id':'8824',
        'address':'1A Central Road',
        'suburb':'Kingsland',
        'lat':'-36.871456',
        'lng':'174.745283'
    },
    {
        'id':'8825',
        'address':'83 West Tamaki Road',
        'suburb':'Glen Innes',
        'lat':'-36.869723',
        'lng':'174.858745'
    },
    {
        'id':'8826',
        'address':'250 Balmoral Road',
        'suburb':'Sandringham',
        'lat':'-36.886140',
        'lng':'174.741856'
    },
    {
        'id':'8827',
        'address':'3 Chalmers Street',
        'suburb':'Avondale',
        'lat':'-36.900555',
        'lng':'174.700972'
    },
    {
        'id':'8828',
        'address':'249 Mount Albert Road',
        'suburb':'Sandringham',
        'lat':'-36.897557',
        'lng':'174.733447'
    },
    {
        'id':'8829',
        'address':'11 McNab Street',
        'suburb':'Penrose',
        'lat':'-36.906321',
        'lng':'174.815787'
    },
    {
        'id':'8830',
        'address':'62­-66 The Drive',
        'suburb':'Epsom',
        'lat':'-36.893094',
        'lng':'174.770305'
    },
    {
        'id':'8831',
        'address':'19 Gavin Street',
        'suburb':'Ellerslie',
        'lat':'-36.903540',
        'lng':'174.815718'
    },
    {
        'id':'8832',
        'address':'111 Mount Wellington Highway',
        'suburb':'Mount Wellington',
        'lat':'-36.903267',
        'lng':'174.841496'
    },
    {
        'id':'8833',
        'address':'36­-38 White Swan Road',
        'suburb':'Mount Roskill',
        'lat':'-36.914553',
        'lng':'174.725712'
    },
    {
        'id':'8834',
        'address':'66­70 Rockfield Road',
        'suburb':'Penrose',
        'lat':'-36.907094',
        'lng':'174.805490'
    },
    {
        'id':'8835',
        'address':'77 Selwyn Street',
        'suburb':'Onehunga',
        'lat':'-36.921070',
        'lng':'174.783175'
    },
    {
        'id':'8836',
        'address':'120 Mays Road',
        'suburb':'Te Papapa',
        'lat':'-36.920803',
        'lng':'174.803248'
    },
    {
        'id':'8837',
        'address':'805 Great South Road',
        'suburb':'Mount Wellington',
        'lat':'-36.927266',
        'lng':'174.833226'
    },
    {
        'id':'8838',
        'address':'115 Carbine Road',
        'suburb':'Mount Wellington',
        'lat':'-36.922898',
        'lng':'174.850732'
    },
    {
        'id':'8839',
        'address':'16 Hans Street',
        'suburb':'Otahuhu',
        'lat':'-36.950247',
        'lng':'174.834082'
    },
    {
        'id':'8840',
        'address':'21­-29 Liverpool Street,',
        'suburb':'Auckland Central',
        'lat':'-36.855923',
        'lng':'174.764379'
    },
    //#8841 is tunnel from Penrose to Hobson St
    {
        'id':'8842',
        'address':'410 Albany Highway',
        'suburb':'Albany',
        'lat':'-36.739003',
        'lng':'174.690364'
    },
    {
        'id':'8843',
        'address':'375 Glenvar Road,',
        'suburb':'Torbay',
        'lat':'-36.699939',
        'lng':'174.723442',
        'exposed':'true'
    },
    {
        'id':'8844',
        'address':'135 McLeod Road',
        'suburb':'Te Atatu South',
        'lat':'',
        'lng':''
    },
    {
        'id':'8845',
        'address':'8 Woodford Avenue',
        'suburb':'Henderson',
        'lat':'',
        'lng':''
    },
    {
        'id':'8846',
        'address':'14 Titirangi Road',
        'suburb':'New Lynn',
        'lat':'-36.910597',
        'lng':'174.677119',
        'exposed':'true'
    },
    {
        'id':'8847',
        'address':'6 Clinker Place',
        'suburb':'New Lynn',
        'lat':'-36.913765',
        'lng':'174.686624',
        'exposed':'true'
    },
    {
        'id':'8848',
        'address':'15 Waikaukau Road',
        'suburb':'Glen Eden',
        'lat':'-36.906923',
        'lng':'174.638681'
    },
    {
        'id':'8849',
        'address':'140­-142 Henderson Valley Road',
        'suburb':'Henderson',
        'lat':'',
        'lng':''
    },
    {
        'id':'8850',
        'address':'45­-49 Keeling Road',
        'suburb':'Henderson',
        'lat':'',
        'lng':''
    },
    {
        'id':'8851',
        'address':'515 South Titirangi Road',
        'suburb':'Titirangi',
        'lat':'-36.939120',
        'lng':'174.654933'
    },
    {
        'id':'8852',
        'address':'647 West Coast Road',
        'suburb':'Oratia',
        'lat':'',
        'lng':''
    },
    {
        'id':'8853',
        'address':'37 Don Buck Road',
        'suburb':'Massey',
        'lat':'',
        'lng':''
    },
    {
        'id':'8854',
        'address':'44­-50 Sabulite Road',
        'suburb':'Kelston',
        'lat':'-36.898836',
        'lng':'174.657703',
        'exposed':'true'
    },
    {
        'id':'8855',
        'address':'28 Royal View Road',
        'suburb':'Te Atatu',
        'lat':'',
        'lng':''
    },
    {
        'id':'8856',
        'address':'1 Trig Road',
        'suburb':'Whenuapai',
        'lat':'',
        'lng':''
    },
    {
        'id':'8857',
        'address':'606 Swanson Road',
        'suburb':'Swanson',
        'lat':'',
        'lng':''
    },
    {
        'id':'8858',
        'address':'173-­183 Triangle Road',
        'suburb':'Massey',
        'lat':'',
        'lng':''
    },
    {
        'id':'8859',
        'address':'86 Atkinson Road',
        'suburb':'Titirangi',
        'lat':'-36.926974',
        'lng':'174.654958',
        'exposed':'true'
    },
    {
        'id':'8860',
        'address':'171 Simpson Road',
        'suburb':'Henderson',
        'lat':'',
        'lng':''
    },
    {
        'id':'8861',
        'address':'449 Huia Road',
        'suburb':'Laingholm',
        'lat':'',
        'lng':''
    },
    {
        'id':'8862',
        'address':'496 Airfield Road',
        'suburb':'Ardmore',
        'lat':'',
        'lng':''
    },
    {
        'id':'8863',
        'address':'6 Smiths Avenue',
        'suburb':'Papakura',
        'lat':'',
        'lng':''
    },
    {
        'id':'8864',
        'address':'32A Red Beach Road',
        'suburb':'Red Beach',
        'lat':'',
        'lng':''
    },
    {
        'id':'8865',
        'address':'1 Wyn Close',
        'suburb':'Kaukapakapa',
        'lat':'',
        'lng':''
    },
    {
        'id':'8866',
        'address':'2 Mahurangi East Road, Snells Beach',
        'suburb':'Snells Beach',
        'lat':'-36.398689',
        'lng':'174.707423'
    },
    {
        'id':'8867',
        'address':'1 Don McKinnon Drive',
        'suburb':'Albany',
        'lat':'-36.725566',
        'lng':'174.706564'
    },
    {
        'id':'8868',
        'address':'179 Bush Road',
        'suburb':'Rosedale',
        'lat':'-36.748241',
        'lng':'174.706309'
    },
    {
        'id':'8869',
        'address':'59 Andersons Road',
        'suburb':'Albany',
        'lat':'-36.716636',
        'lng':'174.723788',
        'exposed':'true'
    },
    {
        'id':'8870',
        'address':'161­-167 Bayswater Avenue',
        'suburb':'Takapuna',
        'lat':'-36.810685',
        'lng':'174.786930',
        'exposed':'true'
    },
    {
        'id':'8871',
        'address':'82A Eskdale',
        'suburb':'Birkenhead',
        'lat':'-36.794869',
        'lng':'174.711502',
        'exposed':'true'
    },
    {
        'id':'8872',
        'address':'5A Balmain Road',
        'suburb':'Birkenhead',
        'lat':'-36.811039',
        'lng':'174.709053'
    },
    {
        'id':'8873',
        'address':'16­-18 Plumer Street',
        'suburb':'Auckland Central',
        'lat':'-36.844732',
        'lng':'174.774445'
    },
    {
        'id':'8874',
        'address':'13­-21 Hobson Street',
        'suburb':'Auckland Central',
        'lat':'-36.845350',
        'lng':'174.762354'
    },
    {
        'id':'8875',
        'address':'497 East Coast Road',
        'suburb':'Mairangi Bay',
        'lat':'-36.733629',
        'lng':'174.734140'
    },
    {
        'id':'8876',
        'address':'27 James Street',
        'suburb':'Glenfield',
        'lat':'-36.770217',
        'lng':'174.720166'
    },
    {
        'id':'8877',
        'address':'220 Kyle Road',
        'suburb':'Greenhithe',
        'lat':'',
        'lng':''
    },
    {
        'id':'8878',
        'address':'1B Jutland Road',
        'suburb':'Hauraki',
        'lat':'-36.797631',
        'lng':'174.778204'
    },
    {
        'id':'8879',
        'address':'18 Highbury Bypass',
        'suburb':'Birkenhead',
        'lat':'-36.810730',
        'lng':'174.722196'
    },
    {
        'id':'8880',
        'address':'2B East Coast Road',
        'suburb':'Milford',
        'lat':'-36.771853',
        'lng':'174.762532'
    },
    {
        'id':'8881',
        'address':'51 Lake Road',
        'suburb':'Northcote',
        'lat':'-36.807340',
        'lng':'174.744574'
    },
    {
        'id':'8882',
        'address':'228 Sunset Road',
        'suburb':'Rosedale',
        'lat':'-36.752340',
        'lng':'174.728962',
        'exposed':'true'
    },
    {
        'id':'8883',
        'address':'33 and 35 Northcote Road',
        'suburb':'Northcote',
        'lat':'',
        'lng':''
    },
    {
        'id':'8884',
        'address':'40A Glenvar Road',
        'suburb':'Torbay',
        'lat':'-36.693642',
        'lng':'174.744574'
    },
    {
        'id':'8885',
        'address':'4A Wairau Road',
        'suburb':'Takapuna',
        'lat':'-36.780502',
        'lng':'174.746796'
    },
    {
        'id':'8886',
        'address':'53 Carlisle Road',
        'suburb':'Browns Bay',
        'lat':'-36.709314',
        'lng':'174.739847'
    },
    {
        'id':'8887',
        'address':'248A East Coast Road',
        'suburb':'Forrest Hill',
        'lat':'-36.755883',
        'lng':'174.750213'
    },
    {
        'id':'8888',
        'address':'29 Huron Street',
        'suburb':'Takapuna',
        'lat':'-36.790060',
        'lng':'174.768110'
    },
    {
        'id':'8889',
        'address':'Pakiri Block Road (adjoins 113)',
        'suburb':'Tomarata',
        'lat':'-36.224079',
        'lng':'174.639282',
    },
    {
        'id':'8890',
        'address':'1423 State Highway 1',
        'suburb':'Wellsford',
        'lat':'-36.320328',
        'lng':'174.540485',
        'exposed':'true'
    },
    {
        'id':'8891',
        'address':'Leigh Road (adjoins No. 937)',
        'suburb':'Big Omaha',
        'lat':'',
        'lng':''
    },
    {
        'id':'8892',
        'address':'Matakana Road (near Golf Road)',
        'suburb':'Warkworth',
        'lat':'',
        'lng':''
    },
    {
        'id':'8893',
        'address':'23 Dawson Road',
        'suburb':'Snells Beach',
        'lat':'-36.428054',
        'lng':'174.726288'
    },
    {
        'id':'8894',
        'address':'140 West Hoe Road',
        'suburb':'Orewa',
        'lat':'',
        'lng':''
    },
    {
        'id':'8895',
        'address':'East Coast Road (near Spur Road)',
        'suburb':'Silverdale',
        'lat':'',
        'lng':''
    },
    {
        'id':'8896',
        'address':'42­-44 Homestead Road',
        'suburb':'Whangaparaoa',
        'lat':'',
        'lng':''
    },
    {
        'id':'8897',
        'address':'91 Gulf Harbour Drive',
        'suburb':'Hobbs Bay',
        'lat':'',
        'lng':''
    },
    {
        'id':'8898',
        'address':'52­56 Awaroa and 139­143 Rautawhiri Roads',
        'suburb':'Helensville',
        'lat':'',
        'lng':''
    },
    {
        'id':'8899',
        'address':'Horseshoe Bush Road (adjoins No. 223)',
        'suburb':'Dairy Flat',
        'lat':'',
        'lng':''
    },
    {
        'id':'8900',
        'address':'Glenmore Road (nears State Highway 17 Coatesville Riverhead Highway)',
        'suburb':'Coatesville',
        'lat':'',
        'lng':''
    },
    {
        'id':'8901',
        'address':'1 Factory Road',
        'suburb':'Waimauku',
        'lat':'',
        'lng':''
    },
    {
        'id':'8902',
        'address':'Riverhead Road (adjoins No. 301)',
        'suburb':'Riverhead',
        'lat':'',
        'lng':''
    },
    {
        'id':'8903',
        'address':'99 Lincoln Street',
        'suburb':'Ponsonby',
        'lat':'-36.854551',
        'lng':'174.739235'
    },
    {
        'id':'8904',
        'address':'505 and 509A State Highway 17 Dairy Flat Highway',
        'suburb':'Lucas Heights',
        'lat':'',
        'lng':''
    }
]
